import { Component } from '@angular/core';

@Component({
  selector: 'tal-root',
  template: `
    
    <div class="container mt-3">
      
      <tal-card 
        title="USERS" 
        [alert]="true" 
        [opened]="userPanelIsOpen" 
        (toggle)="userPanelIsOpen = !userPanelIsOpen"
      >
        <tal-tabbar
          [items]="users"
          [active]="activeUser"
          (tabClick)="setActive($event)"
        ></tal-tabbar>

        <tal-gmap
          [lat]="activeUser?.lat" [lng]="activeUser?.lng"></tal-gmap>

      </tal-card> 
      
      <tal-card title="ANIMALS" [alert]="true" [collapsable]="false">
        <tal-tabbar
          [items]="animals"
          [active]="activeCategory"
          (tabClick)="setActiveCategory($event)"
        ></tal-tabbar>

        <tal-tabbar
          [labelField]="'breed'"
          [items]="activeCategory?.animals"
          (tabClick)="setActiveBreed($event)"
        ></tal-tabbar>

        <tal-list [items]="activeBreed?.list"></tal-list>
        

      </tal-card>
      
    </div>
  `,
})
export class UIkitComponent {
  userPanelIsOpen = false;

  users = [
    { id: 1, url: 'xxxxxx', lat: 42, lng: 13, label: 'Fabio'},
    { id: 2, url: 'xxxxxx', lat: 41, lng: 13, label: 'Gaia'},
    { id: 3, url: 'xxxxxx', lat: 42, lng: 13, label: 'Isotta'},
    { id: 4, url: 'xxxxxx', lat: 43, lng: 13, label: 'Newton'}
  ];

  animals = [
    {
      id: 1,
      label: 'Cats',
      animals: [
        {
          id: 10,
          breed: 'Siamesi',
          list: [
            { id: 1, name: 'mario' },
            {id: 2, name: 'giorgio' }
          ]
        },
        { id: 20, breed: 'Persiani' },
        { id: 30, breed: 'Certosini' }
      ]
    },
    {
      id: 2, label: 'Dogs',
      animals: [
        { id: 10, breed: 'LAbrador' },
        { id: 20, breed: 'Golden' },
      ]
    },
    {
      id: 3, label: 'Horses', animals: []
    },
  ];

  activeUser = this.users[3];
  activeCategory;
  activeBreed;

  setActive(user) {
    this.activeUser = user;
  }

  setActiveCategory(cat) {
    this.activeCategory = cat;
  }

  setActiveBreed(breed) {
    this.activeBreed = breed;
  }

  openSheet(user) {

    window.open('http://www.xyz.com/' + user.id)
  }
}





