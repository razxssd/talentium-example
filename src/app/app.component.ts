import { Component } from '@angular/core';

@Component({
  selector: 'tal-root',
  template: `
      <tal-navbar></tal-navbar>
      <router-outlet></router-outlet>
  `
})
export class AppComponent {

}





