import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}n

  canActivate() {

    const isAuth = this.authService.auth$.getValue().isAuthenticated;

    if (!isAuth) {
      this.router.navigateByUrl('login')
    }

    return isAuth;
  }
}
