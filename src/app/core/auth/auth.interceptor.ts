import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {}

  intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

    const copiedReq = req.clone({
      headers: req.headers.set(
        'Authorization', 'Bearer ' + this.authService.auth$.getValue().token
      )
    });
/*
    console.log('copiedReq!', copiedReq);
    if (!authService.authorized) {
      this.router.navigateByUrl('login');
    }*/

    return next.handle(copiedReq)
  }
}

/**
{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true  }
*/
