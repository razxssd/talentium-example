export class ConfigService {
  private _theme = 'dark';

  get theme() {
    return this._theme;
  }

  set theme(value: string) {
    this._theme = value;
  }
}
