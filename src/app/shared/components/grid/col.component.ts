import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tal-col',
  template: `
    <div style="background: lightgray; border-radius: 20px">
     <ng-content></ng-content>
    </div>
  `
})
export class ColComponent {
  @Input() col;
  @HostBinding('className') get className() {
    return 'col-' + this.col;
  }

}
