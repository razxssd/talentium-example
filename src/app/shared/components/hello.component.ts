import { Component, Input } from '@angular/core';

@Component({
  selector: 'tal-hello',
  template: '<div>Hello {{name}}</div>'
})
export class HelloComponent {
  @Input() name = 'guest';
}
