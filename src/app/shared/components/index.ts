import { HelloComponent } from './hello.component';
import { TabbarComponent } from './tabbar.component';
import { CardComponent } from './card/card.component';
import { ColComponent } from './grid/col.component';
import { RowComponent } from './grid/row.component';
import { GmapComponent } from './gmap.component';
import { CollapsableComponent } from './card/collapsable.component';
import { ListComponent } from './list.component';

export const COMPONENTS = [
  HelloComponent, CardComponent, GmapComponent, TabbarComponent, ListComponent,
  RowComponent, ColComponent, CollapsableComponent,
];
