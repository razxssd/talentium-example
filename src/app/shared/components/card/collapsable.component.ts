import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tal-collapsable',
  template: `
    <div class="card mb-1">
      <div class="card-header"
           (click)="opened = !opened"
      >
        <i
          class="fa"
          [ngClass]="{
             'fa-arrow-circle-down': opened,
             'fa-arrow-circle-right': !opened
          }"
        ></i>
        {{title}}
      </div>
      
      <div class="card-body"
           *ngIf="opened"
      >
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: []
})
export class CollapsableComponent {
  @Input() title;
  opened = true;
}
