export interface Device {
  label: string;
  os: string;
  desc: string;
  id: number;
  lat: number;
  lng: number;
  price: number;
  memory: number;
  rate: number;
  address: Address;
}

export interface Address {
  city: string;
}
