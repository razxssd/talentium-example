import { NgModule } from '@angular/core';
import { DeviceComponent } from './device.component';
import { DeviceListComponent } from './components/device-list.component';
import { OsIconComponent } from './components/os-icon.component';
import { DeviceFormComponent } from './components/device-form.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DeviceService } from './services/device.service';
import { DeviceStore } from './services/device.store';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    DeviceComponent,
    DeviceFormComponent,
    DeviceListComponent,
    OsIconComponent,
  ],
  exports: [

  ],
  imports: [
    SharedModule,
    FormsModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: DeviceComponent},
    ])
  ],
  providers: [
    DeviceService, DeviceStore
  ]
})
export class DeviceModule {}

