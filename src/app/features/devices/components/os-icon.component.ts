import { Component, Input } from '@angular/core';

@Component({
  selector: 'tal-os-icon',
  template: `
    <i class="fa" [ngClass]="{
        'fa-android': os === 'android',
        'fa-apple': os === 'IOS',
        'fa-tablet': os === 'other'
      }"></i>
  `
})
export class OsIconComponent {
  @Input() os: string;
}
