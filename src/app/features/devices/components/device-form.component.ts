import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../model/device';

@Component({
  selector: 'tal-device-form',
  template: `

    <form
      #f="ngForm" (submit)="save.emit(f)">

      <div *ngIf="inputLabel.errors?.required">il campo di input è required</div>
      <div *ngIf="inputLabel.errors?.minlength">
        il campo è troppo corto. Mancano {{5 - inputLabel.errors?.minlength.actualLength}} caratteri

      </div>

      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">
            <i class="fa"
               [style.color]="inputLabel.invalid ? 'red' : 'green'"
               [ngClass]="{
                'fa-check': inputLabel.valid,
                'fa-times': inputLabel.invalid
              }"></i>

          </div>
        </div>
        <input
          type="text"
          name="label"
          [ngModel]="active?.label"
          class="form-control"
          #inputLabel="ngModel"
          required
          minlength="5"
          placeholder="Device Name"
          [style.color]="inputLabel.invalid && f.touched? 'white' : null"
          [style.background]="inputLabel.invalid && f.touched? 'red' : null"
        >
      </div>

      <input
        type="text"
        name="os"
        placeholder="OS"
        [ngModel]="active?.os"
        class="form-control"
      >
      
      <input
        type="text"
        name="price"
        placeholder="Price"
        [ngModel]="active?.price"
        class="form-control"
      >

      <button
        [disabled]="f.invalid"
        type="submit"
        class="btn btn-primary">

        {{active?.id ? 'EDIT' : 'ADD'}}
      </button>

      <button
        class="btn btn-light"
        type="button"
        (click)="reset.emit()"
        *ngIf="active"
      >
        ADD NEW
      </button>
    </form>
  `,
  styles: []
})
export class DeviceFormComponent {
  @Input() active: Device;
  @Output() save: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() reset: EventEmitter<any> = new EventEmitter();


}
