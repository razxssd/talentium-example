import { Device } from '../model/device';

export class DeviceStore {
  active: Device;
  devices: Device[];

  save(result: Device[]) {
    this.devices = result;
  }


  delete(id: string) {
    const index = this.devices.findIndex((d) => d.id === id);
    this.devices.splice(index, 1);
  }

  add(device: Device) {
    this.devices.push(device);
  }


  edit(device: Device) {
    const newDevice = Object.assign({}, device, { id: this.active.id })
    const index = this.devices.findIndex(d => d.id === this.active.id);
    this.devices[index] = newDevice;
  }

}
