import { Component } from '@angular/core';
import { ConfigService } from '../../core/service/config.service';
import { DeviceService } from './services/device.service';
import { DeviceStore } from './services/device.store';

@Component({
  selector: 'tal-devices',
  template: `
    
    <tal-row>
      <tal-col [col]="colSx" >
        <tal-collapsable title="FORM">
          <tal-device-form 
            [active]="store.active"
            (save)="actions.save($event)"
            (reset)="actions.reset()"
          ></tal-device-form>
        </tal-collapsable>
      </tal-col>

      <tal-col [col]="colDx">
        <tal-collapsable title="LIST">
          <tal-device-list 
            [items]="store.devices"
            [active]="store.active"
            (setActive)="actions.setActive($event)"
            (delete)="actions.delete($event)"
          ></tal-device-list>
        </tal-collapsable>
        
      </tal-col>
    </tal-row>
    
    <hr>
    <button (click)="colSx = 12; colDx = 12">Change Layout</button>

    <button (click)="config.theme = 'light' ">light</button>
    <button (click)="config.theme = 'dark' ">dark</button>
  `
]
})
export class DeviceComponent {
  colSx = 4;
  colDx = 8;

  constructor(
    public config: ConfigService,
    public actions: DeviceService,
    public store: DeviceStore
  ) {
    actions.getAll();
  }
}





